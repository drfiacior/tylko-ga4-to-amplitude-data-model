SELECT
-- event type
event_name as event_type,
-- event parameters as JSON structure
TO_JSON(STRUCT(
        
        -- flat ecommerce and device objects
        -- names needs to be specified explicitly to appear in Amplitude event props
        e.ecommerce.purchase_revenue as purchase_revenue,
        e.ecommerce.purchase_revenue_in_usd as purchase_revenue_in_usd,
        e.ecommerce.refund_value as refund_value,
        e.ecommerce.refund_value_in_usd as refund_value_in_usd,
        e.ecommerce.shipping_value_in_usd as shipping_value_in_usd,
        e.ecommerce.tax_value as tax_value,
        e.ecommerce.tax_value_in_usd as tax_value_in_usd,
        e.ecommerce.total_item_quantity as total_item_quantity,
        e.ecommerce.transaction_id as transaction_id,
        e.ecommerce.unique_items as unique_items,
        e.device.advertising_id as advertising_id,
        e.device.browser as browser,
        e.device.browser_version as browser_version,
        e.device.category as category,
        e.device.is_limited_ad_tracking as limited_ad_tracking,
        e.device.language as language,
        e.device.mobile_brand_name as mobile_brand_name,
        e.device.mobile_marketing_name as mobile_marketing_name,
        e.device.mobile_model_name as mobile_model_name,
        e.device.mobile_os_hardware_model as mobile_os_hardware_model,
        e.device.operating_system as operating_system,
        e.device.operating_system_version as operating_system_version,
        e.device.time_zone_offset_seconds as time_zone_offset_seconds,
        e.device.vendor_id as vendor_id,
        e.device.web_info.browser as browser,
        e.device.web_info.browser_version as browser_version,
        e.device.web_info.hostname as hostname,
        
        -- extracting values for the first item in the array, as only one item was added to cart in the majority of cases
        -- this needs to be done to get the price information
        items[SAFE_OFFSET(0)].item_brand as item_brand,
        items[SAFE_OFFSET(0)].item_category as item_category,
        items[SAFE_OFFSET(0)].item_id as item_id,
        items[SAFE_OFFSET(0)].item_variant as item_variant,
        items[SAFE_OFFSET(0)].price as price,

        -- ab test parameters on the event parsing
        regexp_extract((SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ab_test'), r'blind:([^,]*)') as test_blind,
        regexp_extract((SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ab_test'), r'ff_c_high_v3:([^,]*)') as test_ff_c_high_v3,
        regexp_extract((SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ab_test'), r'ff_c_high_v2:([^,]*)') as test_ff_c_high_v2,
        regexp_extract((SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ab_test'), r'feature_items:([^,]*)') as test_feature_items,
        regexp_extract((SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ab_test'), r'feature_samples:([^,]*)') as test_feature_samples,
        regexp_extract((SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ab_test'), r'pinterest_sharing_btn:([^,]*)') as test_pinterest_sharing_btn,
        regexp_extract((SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ab_test'), r'is_grid_v3:([^,]*)') as test_is_grid_v3,

        -- complete ab test string for reference
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ab_test') AS ab_test,

        -- session
        (SELECT value.int_value FROM UNNEST(event_params) AS param WHERE param.key = 'ga_session_id') AS ga_session_id,
        (SELECT value.int_value FROM UNNEST(event_params) AS param WHERE param.key = 'ga_session_number') AS ga_session_number,
        
        -- utms
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'source') AS source,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'medium') AS medium,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'content') AS content,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'term') AS term,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'campaign') AS campaign,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'campaign_id') AS campaign_id,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'gclid') AS gclid,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'dclid') AS dclid,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'gclsrc') AS gclsrc,

        -- page
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'page_title') AS page_title,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'page_location') AS page_location,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'page_type') AS page_type,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'virtual_page') AS virtual_page,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'contentGroup') AS contentGroup,

        -- eventParams
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'eventParam_0') AS eventParam_0,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'eventParam_1') AS eventParam_1,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'eventParam_2') AS eventParam_2,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'eventParam_3') AS eventParam_3,

        -- referrer
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'page_referrer') AS page_referrer,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'ignore_referrer') AS ignore_referrer,

        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'product_type') AS product_type,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'currency') AS currency,
        (SELECT value.int_value FROM UNNEST(event_params) AS param WHERE param.key = 'engagement_time_msec') AS engagement_time_msec,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'debug_mode') AS debug_mode,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'engaged_session_event') AS engaged_session_event,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'traffic_type') AS traffic_type,
        (SELECT value.string_value FROM UNNEST(event_params) AS param WHERE param.key = 'session_engaged') AS session_engaged
    )) as event_properties,
    
    -- no event timestamp in the source data, using event_date instead
UNIX_MILLIS(TIMESTAMP(event_date)) as time,

-- created at required by amplitude, setting fixed for 2023-06-29
TIMESTAMP('2023-06-29') as created_at,

-- joined from inter_ga_users view
e.user_id,
u.user_properties

-- source table was copied to my private project
FROM `tylko.events` e
left join `tylko.inter_ga_users` u
on e.user_id = u.user_id
and e.event_date = u.date
-- limit to be used for optimized modeling.
--LIMIT 100
