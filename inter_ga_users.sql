with
  -- long table of user properties 
  user_ab_tests_long AS (
    select
      t.date,
      t.dimension1 as user_id,
      SPLIT(abt, ':')[OFFSET(0)] as abt_key,
      SPLIT(abt, ':')[OFFSET(1)] as abt_value
    from tylko.ga_user_abtest t,
    unnest(split(t.dimension16)) abt
  ),
  -- reference table of user property keys
  ab_tests_keys as (
    select distinct
      abt_key
    from user_ab_tests_long
  ),
  -- wide table of user properties
  user_ab_tests_wide as (
    select * from user_ab_tests_long
    pivot (
      MAX(user_ab_tests_long.abt_value) as test
      FOR abt_key in ("blind", "pinterest_sharing_btn", "is_grid_v3", "feature_samples", "cro_add_to_cart_new_ds_button", "cro_reviewgallery", "is_grid_v2", "fast_delivery", "cro_hp_video_hero_mobile", "s4l_new_renders", "cro_contactform_newsletter_signin_test", "cro_hp_video_hero", "ff_preview_test_hero", "cro_payment_methods_tab_collapsed_test", "ff_valentines_promo", "cro_quick_order_status_check_form", "cro_hp_hero_video_vs_static", "cro_new_get_app", "cro_klarna", "cro_megamenu_spaces_test", "feature_items", "cro_rooms_sanity_cms_test", "ff_c_high_v3", "ff_c_high_v2", "cro_crow_2", "feature_cplus_depth_t02", "t02_matte_black_enabled", "cro_hp_spaces_minigrid", "T02_new_colors_feature_flag", "ff_triple_image_hero", "cro_checkout_newsletter_signin_test", "cro_grid_color_swatch_to_pdp", "cplus_disable_local_edge", "ff_hp_hero_icons", "hp_minigrid", "cro_a2c_mobile_primary", "ff_sku_configurator", "grid_test_splus_aggresive", "cro_newcart", "ff_c_high_t01", "cro_crow", "cro_hero_de", "cplus_new_slider_2", "cro_shorter_pdp_v2", "covid_popup", "T02_swatch_colors_order_test", "is_grid_color_swatch", "t02_matte_black_3d_landing_page", "cro_grid_color_swatch", "cro_newtype01ply", "cro_shoerack", "cro_elevator_v3", "cro_new_grid_swatch", "cro_megamenu", "cro_shorter_pdp", "cro_new_adyen", "cro_checkout_new", "cro_instagrid", "cro_checkout_new_de")
    )
  )
-- final table with user properties expanded to JSON structure
select
date,
user_id,
to_json(struct(test_blind, test_pinterest_sharing_btn, test_is_grid_v3, test_feature_samples, test_cro_add_to_cart_new_ds_button, test_cro_reviewgallery, test_is_grid_v2, test_fast_delivery, test_cro_hp_video_hero_mobile, test_s4l_new_renders, test_cro_contactform_newsletter_signin_test, test_cro_hp_video_hero, test_ff_preview_test_hero, test_cro_payment_methods_tab_collapsed_test, test_ff_valentines_promo, test_cro_quick_order_status_check_form, test_cro_hp_hero_video_vs_static, test_cro_new_get_app, test_cro_klarna, test_cro_megamenu_spaces_test, test_feature_items, test_cro_rooms_sanity_cms_test, test_ff_c_high_v3, test_ff_c_high_v2, test_cro_crow_2, test_feature_cplus_depth_t02, test_t02_matte_black_enabled, test_cro_hp_spaces_minigrid, test_T02_new_colors_feature_flag, test_ff_triple_image_hero, test_cro_checkout_newsletter_signin_test, test_cro_grid_color_swatch_to_pdp, test_cplus_disable_local_edge, test_ff_hp_hero_icons, test_hp_minigrid, test_cro_a2c_mobile_primary, test_ff_sku_configurator, test_grid_test_splus_aggresive, test_cro_newcart, test_ff_c_high_t01, test_cro_crow, test_cro_hero_de, test_cplus_new_slider_2, test_cro_shorter_pdp_v2, test_covid_popup, test_T02_swatch_colors_order_test, test_is_grid_color_swatch, test_t02_matte_black_3d_landing_page, test_cro_grid_color_swatch, test_cro_newtype01ply, test_cro_shoerack, test_cro_elevator_v3, test_cro_new_grid_swatch, test_cro_megamenu, test_cro_shorter_pdp, test_cro_new_adyen, test_cro_checkout_new, test_cro_instagrid, test_cro_checkout_new_de)) as user_properties
from user_ab_tests_wide
